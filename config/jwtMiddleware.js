var jwt         = require('jsonwebtoken');
module.exports = function(apiRoutes) {
    apiRoutes.use(function (req, res, next) {

        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {

            // verifies secret and checks exp
            jwt.verify(token, req.app.get('superSecret'), function (err, decoded) {
                if (err) {
                    return res.status(401).send({
                        data: null,
                        error: {
                            message : 'Failed to authenticate token.',
                            code : 401
                        }
                    });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    req.user = decoded._doc;
                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                data: null,
                error: {
                    message : 'No token provided.',
                    code : 403
                }
            });

        }
    });
}