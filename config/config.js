var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'newnodeproject'
    },
    ipaddress: '127.0.0.1',
    port: 3000,
    db: 'mongodb://localhost/test', 
    secret: 'ilovescotchyscotch'
  },

  test: {
    root: rootPath,
    app: {
      name: 'newnodeproject'
    },
    ipaddress: process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
    port: process.env.PORT || 3000,
    db: process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://localhost/test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'newnodeproject'
    },
    ipaddress: process.env.OPENSHIFT_NODEJS_IP,
    port: process.env.OPENSHIFT_NODEJS_PORT,
    db: process.env.OPENSHIFT_MONGODB_DB_URL,
    secret: 'ilovescotchyscotch'
  }
};

module.exports = config[env];
