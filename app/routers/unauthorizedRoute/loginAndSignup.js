var User        = require('../../models/user');
var jwt         = require('jsonwebtoken')

module.exports = function(apiRoutes) {
    // route to authenticate a user (POST http://localhost:8080/api/authenticate)

    apiRoutes.post('/user/login', function(req, res) {
        User.findOne({
            email : req.body.email
        }, function(err, user) {

            if (err) throw err;
            if (user) {
                // check if password matches
                user.comparePassword(req.body.password, function (err, isMatch) {
                    if (isMatch && !err) {
                        // if user is found and password is right create a token
                        var token = jwt.sign(user, req.app.get('superSecret'), {
                            expiresIn: 1440 // expires in 24 hours
                        });

                        // return the information including token as JSON
                        res.json({
                            data: {
                                user: user,
                                token: token
                            },
                            error: null
                        });
                    } else {
                        res.status(400).send({
                            data: null,
                            error: {
                                message: "Authentication failed. User not found.",
                                code : 400
                            }
                        });
                    }
                });
            } else {
                res.status(400).send({
                    data: null,
                    error: {
                        message: "Authentication failed. User not found.",
                        code : 400
                    }
                });
            }
        });
    });

    apiRoutes.post('/user/signup', function (req, res) {
        if (!req.body.email || !req.body.fullName || !req.body.password) {
            res.status(400).send({
                data: null,
                error: {
                    message: "Please provide email, full name, and password.",
                    code : 400
                }
            });
        } else {
            var newUser = new User({
                fullName : req.body.fullName,
                email : req.body.email,
                password : req.body.password
            });

            // save the user
            newUser.save(function (err) {
                if (err) {
                    return res.status(400).send({
                        data: null,
                        error: {
                            message: "Email already exists.",
                            code : 400
                        }
                    });
                }

                var token = jwt.sign(newUser, req.app.get('superSecret'), {
                    expiresIn: 1440 // expires in 24 hours
                });

                res.json({
                    data: {
                        user: newUser,
                        token: token
                    },
                    error: null
                });
            });
        }
    });
}