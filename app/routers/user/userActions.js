var User        = require('../../models/user');
var multer      = require('multer')
var upload      = multer({ dest: 'temp/' })
var type        = upload.single('image');
var fs          = require('fs');


module.exports = function(apiRoutes, jwt) {

    apiRoutes.put('/user/personalInfo', function (req, res) {

        var user = req.user;
        user.height = req.body.height;
        user.gender = req.body.gender;
        user.dateOfBirth = req.body.dateOfBirth;
        user.goalWeight = req.body.goalWeight;

        user.save(function (err) {
            if (err) {
                return res.status(500).send({
                    data: null,
                    error: {
                        message: "Please try again.",
                        code : 500
                    }
                });
            }
            res.json({
                data: {
                    user: user
                },
                error: null
            });
        });
    });

    apiRoutes.post('/user/updateImage', type, function(req, res){
        var user = req.user;
        var tmp_path = req.file.path;
        var target_path = 'uploads/' + Date.now() + req.file.originalname;
        var src = fs.createReadStream(tmp_path);
        var dest = fs.createWriteStream(target_path);
        src.pipe(dest);
        src.on('end', function() {
            user.profileImage = target_path;
            User.update({ _id: user._id }, { $set: { profileImage: target_path }}, function (err) {
                if (err)
                    return res.status(500);
                return res.status(200);
            });

        });
        src.on('error', function() {
            res.status(500);
        });
    });
}