
module.exports = function(apiRoutes) {
    apiRoutes.get('/user/auth', function (req, res) {
        console.log(req.user);
        res.json({
            data: {
                user: req.user
            },
            error: null
        });
    })
}