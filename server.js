var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var glob        = require("glob");
var cors        = require('cors');
var jwt         = require('jsonwebtoken');

var config      = require('./config/config');
var port        = process.env.PORT || 8080;
var env = process.env.NODE_ENV || 'development';
app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';


// get our request parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// log to console
app.use(morgan('dev'));

// connect to database
mongoose.Promise = global.Promise;
mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
    throw new Error('unable to connect to database at ' + config.db);
});

// pass passport for configuration
// require('./config/passport')(passport);
app.use(cors({origin: 'http://localhost:9000'}));

// bundle our routes
var apiRoutes = express.Router();
//routes without authentication
require('./app/routers/unauthorizedRoute/loginAndSignup')(apiRoutes);

app.set('superSecret', config.secret);
require('./config/jwtMiddleware')(apiRoutes);

//routes with authentication
var controllers = glob.sync(config.root + '/app/routers/user/*.js');
controllers.forEach(function (controller) {
    require(controller)(apiRoutes);
});

// connect the api routes under /api/*
app.use('/api', apiRoutes);


app.listen(config.port, config.ipaddress, function() {
    console.log('Express server listening on port ' + config.port);
});

module.exports = app;